<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ProcessPlan extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'customer',
        'order_type',
        'code',
        'desc',
    ];
 
    public function outgoing_products(): HasMany
    {
        return $this->hasMany(OutgoingProduct::class);
    }
}
