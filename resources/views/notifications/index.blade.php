<!DOCTYPE html>
<html>
<head>
    <title>Notifications</title>
    <!-- Include your CSS and JavaScript dependencies here -->
</head>
<body>
    <!-- ... rest of your HTML ... -->
<button id="send-notification-btn">Send Notification</button>
<!-- ... rest of your HTML ... -->

    <h1>Notifications</h1>
    <ul id="notifications-list">
        <!-- Notifications will be added dynamically here -->
    </ul>
    
    <script src="{{ asset('js/app.js') }}"></script>
    <script>

    </script>
</body>
</html>
