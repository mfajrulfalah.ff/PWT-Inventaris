/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

import axios from "axios";
window.axios = axios;

window.axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

import Echo from "laravel-echo";
import Pusher from "pusher-js";
import Chart from "chart.js/auto";
import Swal from "sweetalert2";
import moment from "moment";

import "sweetalert2/src/sweetalert2.scss";
import "datatables.net-select-bs4";
import "datatables.net-searchpanes-bs4";
import "datatables.net-responsive-dt";

window.Swal = Swal;
window.Chart = Chart;
window.Pusher = Pusher;
window.moment = moment;

window.Echo = new Echo({
    broadcaster: "pusher",
    key: import.meta.env.VITE_PUSHER_APP_KEY,
    cluster: import.meta.env.VITE_PUSHER_APP_CLUSTER,
    forceTLS: true,
});
